package xa.batch331.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import xa.batch331.Services.ProductService;
import xa.batch331.models.Category;
import xa.batch331.models.Product;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ProductRestController {

    @Autowired
    private ProductService productService;

    @GetMapping("/product")
    public ResponseEntity<List<Product>> getAllProduct(){
        try {
            List<Product> products = this.productService.getAllProduct();
            return new ResponseEntity<>(products, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/product/{id}")
    //ResponseEntity<?> pakai tanda tanya untuk opsional, jadi bisa apa saja
    public ResponseEntity<Object> getCategoryById(@PathVariable("id")Long id){
        try {
            Optional<Product> product = this.productService.getProductById(id);
            if (product.isPresent()){
                return new ResponseEntity<>(product.get(), HttpStatus.OK);
            }else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/product")
    public ResponseEntity<Product> saveProduct(@RequestBody Product product){
        try {
            this.productService.saveProduct(product);
            return new ResponseEntity<>(product, HttpStatus.OK);
        }catch (Exception e){
            Map<String, String> result = new HashMap<>();
            result.put("Message", e.getMessage());
            result.put("Data", null);
            return new ResponseEntity(result,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/product/{id}")
    public ResponseEntity<?> editProduct(@RequestBody Product product, @PathVariable("id") Long id){
        try {
            Optional<Product> productCek = this.productService.getProductById(id);
            if (productCek.isPresent()){
                product.setId(id);
                this.productService.saveProduct(product);
                Map<String, Object> result = new HashMap<>();
                result.put("Message", "Edit data berhasil (OK - 200)");
                result.put("Data", product);
                return new ResponseEntity<>(result, HttpStatus.OK);
            }else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        }catch (Exception e){
            Map<String, String> result = new HashMap<>();
            result.put("Message", e.getMessage());
            result.put("Data", null);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/product/{id}")
    public ResponseEntity<?> deleteProduct (@PathVariable("id")Long id){
        try {
            Optional<Product> product = this.productService.getProductById(id);
            if (product.isPresent()){
                this.productService.deleteProduct(id);
                Map<String, Object> result = new HashMap<>();
                result.put("Message", "Delete data berhasil (OK - 200)");
                return ResponseEntity.status(HttpStatus.OK).body("Product deleted");
            }else {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Product not found");
            }
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

}
