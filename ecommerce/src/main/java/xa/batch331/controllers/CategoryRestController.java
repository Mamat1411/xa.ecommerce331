package xa.batch331.controllers;

import com.fasterxml.jackson.core.JsonParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import xa.batch331.Services.CategoryService;
import xa.batch331.models.Category;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/api")
@CrossOrigin("*")
public class CategoryRestController {

    @Autowired
    private CategoryService categoryService;

    @GetMapping("/category")
    public ResponseEntity<List<Category>> getAllCategory(){
        try {
            List<Category> category =  this.categoryService.getAllCategory();
            // Eksplorasi Stream karena lebih singkat untuk menghitung data yang terdapat di list
            List<Category> count = category.stream().filter(panjang -> panjang.getName().length() > 3).toList();

            return new ResponseEntity<>(category, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/categoryvalue")
    public ResponseEntity<List<Map<String, Object>>> getCategoryValue(){
        try {
            List<Map<String, Object>> category =  this.categoryService.getCategoryValue();
            return new ResponseEntity<>(category, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

//    @GetMapping("/category/{id}")
//    public ResponseEntity<?> getCategoryById(@PathVariable("id")Long id){
//        try {
//            Category category = this.categoryService.getCategoryById(id);
//            return new ResponseEntity<>(category, HttpStatus.OK);
//        }catch (Exception e){
//            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
//        }
//    }

    @GetMapping("/category/{id}")
    //ResponseEntity<?> pakai tanda tanya untuk opsional, jadi bisa apa saja
    public ResponseEntity<Object> getCategoryById(@PathVariable("id")Long id){
        try {
            Optional<Category> category = this.categoryService.getCategoryById(id);
            if (category.isPresent()){
                return new ResponseEntity<>(category.get(), HttpStatus.OK);
            }else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/category")
    public ResponseEntity<Category> saveCategory(@RequestBody Category category){
        try {
            this.categoryService.simpanCategory(category);
            return new ResponseEntity<>(category, HttpStatus.OK);
        }catch (Exception e){
            Map<String, String> result = new HashMap<>();
            result.put("Message", e.getMessage());
            result.put("Data", null);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/category/{id}")
    public ResponseEntity<?> editCategory(@RequestBody Category category, @PathVariable("id") Long id){
        try {
            Optional<Category> categoryCek = this.categoryService.getCategoryById(id);
            if (categoryCek.isPresent()){
                category.setId(id);
                this.categoryService.simpanCategory(category);
                Map<String, Object> result = new HashMap<>();
                result.put("Message", "Edit data berhasil (OK - 200)");
                result.put("Data", category);
                return new ResponseEntity<>(result, HttpStatus.OK);
            }else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        }catch (Exception e){
            Map<String, String> result = new HashMap<>();
            result.put("Message", e.getMessage());
            result.put("Data", null);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/category/{id}")
    public ResponseEntity<?> deleteCategory (@PathVariable("id")Long id){
        try {
            Optional<Category> category = this.categoryService.getCategoryById(id);
            if (category.isPresent()){
                this.categoryService.deleteCategory(id);
                Map<String, Object> result = new HashMap<>();
                result.put("Message", "Delete data berhasil (OK - 200)");
                return ResponseEntity.status(HttpStatus.OK).body("Category deleted");
            }else {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Category not found");
            }
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

}
