package xa.batch331.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import xa.batch331.models.Product;

@Repository
public interface ProductRepo extends JpaRepository<Product, Long> {

}
