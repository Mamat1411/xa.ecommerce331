package xa.batch331.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import xa.batch331.models.Category;

import java.util.List;
import java.util.Map;

@Repository
public interface CategoryRepo extends JpaRepository<Category, Long> {

    @Query(value = "SELECT *  FROM categories c JOIN products p ON products.category_id = categories.id", nativeQuery = true)
    List<Map<String, Object>> getCategoryValue();

}
