package xa.batch331.Services;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xa.batch331.models.Product;
import xa.batch331.repositories.ProductRepo;

import java.util.List;
import java.util.Optional;

@Service
public class ProductService {

    @Autowired
    private ProductRepo productRepo;

    public List<Product> getAllProduct(){
        return this.productRepo.findAll();
    }

    public void saveProduct(Product product){
        this.productRepo.save(product);
    }

    public Optional<Product> getProductById(Long id){
        return this.productRepo.findById(id);
    }

    public void deleteProduct(Long id){
        this.productRepo.deleteById(id);
    }

}
