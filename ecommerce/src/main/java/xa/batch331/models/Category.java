package xa.batch331.models;

import jakarta.annotation.Generated;
import jakarta.persistence.*;

@Entity
//penaman tabel angular
@Table(name = "categories")
public class Category {

    //Penamaan kolom Singular
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long Id;

    @Column(name = "name", length = 30)
    private String Name;

    @Column(name = "description", length = 70)
    private String Description;

    @Column(name="file_path", length = 200)
    private String FilePath;

    @Lob
    @Column(name = "file_content")
    private byte[] FileContent;

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getFilePath() {
        return FilePath;
    }

    public void setFilePath(String filePath) {
        FilePath = filePath;
    }

    public byte[] getFileContent() {
        return FileContent;
    }

    public void setFileContent(byte[] fileContent) {
        FileContent = fileContent;
    }
}
