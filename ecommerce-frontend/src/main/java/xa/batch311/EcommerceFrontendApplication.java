package xa.batch311;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EcommerceFrontendApplication {

	public static void main(String[] args) {
		SpringApplication.run(EcommerceFrontendApplication.class, args);
	}

}
